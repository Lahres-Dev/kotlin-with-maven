package info.lahres.template.kotlinmaven.impl

import info.lahres.template.kotlinmaven.api.dataobjects.Input
import info.lahres.template.kotlinmaven.api.enums.Gender

fun main() {
    val person = Input(name = "Jack", age = 22, gender = Gender.MALE)
    println("Hello ${person.name}. You're ${person.age} years old and you're gender is ${person.gender}.")
}
