[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=Lahres-Dev_kotlin-with-maven&metric=alert_status)](https://sonarcloud.io/dashboard?id=Lahres-Dev_kotlin-with-maven)

## Introduction
This repository serves as template for kotlin projects which use maven as build tool. Features are:

 * Sample kotlin class file with simple output.
 * Multi module maven project
 * Static code analysis with Sonar -> [Project Page](https://sonarcloud.io/dashboard?id=Lahres-Dev_kotlin-with-maven)
 * Self-contained jar file creation
 * Unit tests. Results, created by JaCoCo, are found on sonar project page.
 * Sample license and readme file
 * Git as version control system with .gitignore file to exclude automatically created files from repository.

---

## Building the project
Just run
<pre>
mvn clean package
</pre>
The output is found inside the target folder.

---

## Generating Sonar report
To generate a sonar report, the following maven goal has to be run
<pre>
mvn clean verify -Dsonar.login=$SONAR_LOGIN_TOKEN
</pre>
The sonar login token can be generated on the sonar account page (needs admin rights).

---

## Running the self-contained jar file
To run the self-contained jar file
<pre>
java -jar hello-world-app/target/hello-world-app-1.0-SNAPSHOT-jar-with-dependencies.jar
</pre>
Output should be:
<pre>
Hello Jack. You're 22 years old and you're gender is male.
</pre>

---

## References
[Setting up pom.xml](https://kotlinlang.org/docs/reference/using-maven.html)

Current plugin versions:

 * [kotlin-stdlib](https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-stdlib)
 * [annotations](https://mvnrepository.com/artifact/org.jetbrains/annotations)
 * [kotlin-maven-plugin](https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-maven-plugin)
 * [maven-jar-plugin](https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-jar-plugin)
 * [maven-dependency-plugin](https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-dependency-plugin)
 * [maven-assembly-plugin](https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-assembly-plugin)
 * [junit](https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-api)
 * [assertj-core](https://mvnrepository.com/artifact/org.assertj/assertj-core)
 * [sonar-maven-plugin](https://mvnrepository.com/artifact/org.sonarsource.scanner.maven/sonar-maven-plugin)
 * [jacoco](https://mvnrepository.com/artifact/org.jacoco/org.jacoco.agent)
