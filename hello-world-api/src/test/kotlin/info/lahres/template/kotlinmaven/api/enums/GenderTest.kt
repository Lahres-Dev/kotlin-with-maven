package info.lahres.template.kotlinmaven.api.enums

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

internal class GenderTest {

    @Test
    fun testGender() {
        Assertions.assertThat(Gender.FEMALE.toString()).isEqualTo("female")
        Assertions.assertThat(Gender.MALE.toString()).isEqualTo("male")
    }

}